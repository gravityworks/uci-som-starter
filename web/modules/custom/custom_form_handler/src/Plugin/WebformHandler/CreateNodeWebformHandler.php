<?php

namespace Drupal\custom_form_handler\Plugin\WebformHandler;

use Drupal\node\Entity\Node;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Create a new node entity from a webform submission.
 *
 * @WebformHandler(
 *   id = "Create a node",
 *   label = @Translation("Create a node"),
 *   category = @Translation("Entity Creation"),
 *   description = @Translation("Creates a new node from Webform Submissions."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 * )
 */
class CreateNodeWebformHandler extends WebformHandlerBase {
  /**
   * {@inheritdoc}
   */

  /**
   * Function to be fired after submitting the Webform.
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // Get an array of the values from the submission.
    $values = $webform_submission->getData();
    date_default_timezone_set('UTC');
    \Drupal::logger('create_node')->warning('<pre><code>' . print_r($values, TRUE) . '</code></pre>');

    $details = Paragraph::create([
      'type' => 'rich_text_field',
      'field_rich_text' => [
        'value' => $values['details'],
        'format' => 'full_html',
      ],
    ]);

    // Event Types.
    $event_types = [];
    foreach ($values['event_type'] as &$type) {
      array_push($event_types, ['target_id' => $type]);
    }

    $start_date = strtotime($values['start_date']);
    $end_date = strtotime($values['end_date']);
    \Drupal::logger('create_node')->warning('<pre><code>' . print_r($start_date, TRUE) . '</code></pre>');

    $registration_deadline = strtotime($values['registration_deadline']);
    // Speakers.
    $speakers = [];
    foreach ($values['speakers'] as &$speaker) {

      $item = Paragraph::create([
        'type' => 'event_speaker',
        'field_about' => $speaker['about'],
        // 'field_image' => $speaker_image,
        'field_link' => $speaker['link'],
        'field_title' => $speaker['name'],
        'field_speaker_titles' => $speaker['titles'],
      ]);
      array_push($speakers, $item);
    }
    // Parking.
    $callout_map = Paragraph::create([
      'type' => 'callout_map',
      'field_address_1' => $values['address_1'],
      'field_address_2' => $values['address_2'],
      'field_city' => $values['city'],
      'field_state' => $values['state'],
      'field_zip' => $values['zip'],
      'field_name' => $values['name'],
      // 'field_iframe' => $values['iframe'],
    ]);
    // Sponsors.
    $sponsors = [];
    foreach ($values['sponsors'] as &$sponsor) {
      $item = Paragraph::create([
        'type' => 'event_sponsor',
        'field_is_title_sponsor' => $sponsor['is_title_sponsor'],
        'field_link' => $sponsor['link'],
        // 'field_image' => $sponsor_logo,
        'field_body' => $sponsor['summary'],
        'field_title' => $sponsor['title'],
      ]);
      array_push($sponsors, $item);
    }
    $node_args = [
      'type' => 'event',
      'langcode' => 'en',
      'created' => time(),
      'changed' => time(),
      'uid' => 1,
      'status' => 0,
      'title' => $values['title'],
      'field_location' => $values['location'],
      'field_event_home' => $details,
      'field_contact' => $values['contact'],
      'field_departments' => $values['departments'],
      'field_start_date' => gmdate('Y-m-d\TH:i:s', $start_date),
      'field_end_date' => gmdate('Y-m-d\TH:i:s', $end_date),
      'field_event_cost' => $values['cost'],
      'field_event_type' => $event_types,
      // 'field_hero' => $hero,
      // 'field_teaser_thumbnail' => $thumbnail_image,
      'field_parking' => $callout_map,
      'field_registration' => [
        'uri' => $values['cta_url']['url'],
        'title' => $values['event_call_to_action'],
      ],
      'field_reg' => gmdate('Y-m-d', $registration_deadline),
      'field_summary' => $values['summary'],
      'field_speakers' => $speakers,
      'field_sponsors' => $sponsors,
      'field_agenda_items' => $values['agenda_items'],
    ];
    $node = Node::create($node_args);
    $node->save();
  }

}

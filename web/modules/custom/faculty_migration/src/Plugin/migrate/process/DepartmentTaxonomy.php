<?php

namespace Drupal\faculty_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * Returns Taxonomy Term string based on departmentPrimary and divisionPrimary.
 *
 * @MigrateProcessPlugin(
 * id = "get_department_taxonomy")
 */
class DepartmentTaxonomy extends ProcessPluginBase {

  /**
   * Combines appropriate fields when necessary.
   */
  public function getTerm($value, MigrateExecutableInterface $migrate_executable) {
    if ($value[0] == $value[1]) {
      return $value[0];
    }
    else {
      return $value[0] . ' - ' . $value[1];
    }
  }

}

<?php

namespace Drupal\faculty_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * Returns long string wrapped in html <p> tags.
 *
 * @MigrateProcessPlugin(
 * id = "paragraph_wrap")
 */
class ParagraphWrap extends ProcessPluginBase {

  /**
   * Separates mutiple lines into multiple items.
   *
   * See https://stackoverflow.com/questions/14905305/new-line-to-p-in-php.
   */
  public function wrapInParagraphTags($value, MigrateExecutableInterface $migrate_executable) {
    $stringWithPs = str_replace("\n", "</p>\n<p>", $value);
    return "<p>" . $stringWithPs . "</p>";
  }

}

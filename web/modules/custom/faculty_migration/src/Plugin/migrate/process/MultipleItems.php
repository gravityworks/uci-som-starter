<?php

namespace Drupal\faculty_migration\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;

/**
 * Returns array delimited by a line break.
 *
 * @MigrateProcessPlugin(
 * id = "multiple_items")
 */
class MultipleItems extends ProcessPluginBase {

  /**
   * Separates mutiple lines into multiple items.
   */
  public function generateArray($value, MigrateExecutableInterface $migrate_executable) {
    return array_map('trim', explode("\n", $value));
  }

}

<?php

/**
 * @file
 * Provide custom view sort.
 */

/**
 * Implements hook_views_data_alter().
 */
function event_sort_views_data_alter(array &$data) {
  $data['node__field_start_date']['event'] = [
    // Title for relationship when selecting via the modal.
    'title' => t('Custom Event Sort'),
    // The name of the group to which this relationship belongs.
    'group' => t('Content'),
    // Additional descriptive information.
    'help' => t('Sort events by past/future, then distance from now.'),
    'sort' => [
      'field' => 'field_start_date_value',
      'id' => 'event_sort',
    ],
  ];
}

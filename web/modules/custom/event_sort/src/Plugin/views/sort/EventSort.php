<?php

namespace Drupal\event_sort\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\Date;

/**
 * Custom sort handler for Events.
 *
 * @ViewsSort("event_sort")
 */
class EventSort extends Date {
  /**
   * {@inheritDoc}
   */

  /**
   * Called to add the sort to a query.
   */
  public function query() {
    $this->ensureMyTable();
    $date_alias = "UNIX_TIMESTAMP($this->tableAlias.$this->realField)";

    \Drupal::logger('event_sort')->notice($date_alias);

    // Is the event in the past?
    $this->query->addOrderBy(
      NULL,
      "UNIX_TIMESTAMP() > $date_alias",
      $this->options['order'],
      "in_past"
    );

    // How far in the past/future is this event?
    $this->query->addOrderBy(
      NULL,
      "ABS($date_alias - UNIX_TIMESTAMP())",
      $this->options['order'],
      "distance_from_now"
    );
  }

}
